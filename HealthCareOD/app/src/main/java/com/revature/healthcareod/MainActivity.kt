package com.revature.healthcareod

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.revature.healthcareod.datahandlers.Users


class MainActivity : AppCompatActivity() {
    private val tempUser = Users()


    override fun onCreate(savedInstanceState: Bundle?) {

       // tempUser.readUsers(this)//get the users list from data

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> logout()
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStop() {
        //tempUser.writeUsers() //write the users list when app is turned off
        super.onStop()
    }

    fun logout(): Boolean {
        val intent = intent
        finish()
        startActivity(intent)
        return true
    }

}