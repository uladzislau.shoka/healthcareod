package com.revature.healthcareod.datahandlers//Anderson Adams, 3/23/21
//Banking Kotlin application
//data class made for serialization
import kotlinx.serialization.Serializable

@Serializable
data class FileHandle(val customers : HashMap<String, NurseAccount>,
                      val transactions: ArrayList<String>) {

}