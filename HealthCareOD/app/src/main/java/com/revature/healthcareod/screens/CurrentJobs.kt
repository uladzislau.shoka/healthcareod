package com.revature.healthcareod.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.databinding.FragmentCurrentJobsBinding
import com.revature.healthcareod.databinding.FragmentFindJobsBinding
import com.revature.healthcareod.datahandlers.CurrentJobsAdapter
import com.revature.healthcareod.datahandlers.JobFinder
import com.revature.healthcareod.datahandlers.NurseAccount
import com.revature.healthcareod.datahandlers.Users

class CurrentJobs : Fragment() {

    private var _binding: FragmentCurrentJobsBinding?=null
    private val binding get()=_binding!!
    lateinit var userName:String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        println("The view is being created")
        super.onCreate(savedInstanceState)

        arguments?.let{
            println("user name being assigned")
            userName=it.getString("user").toString()
        }
        if(userName!=null){
            println("user being assigned")
            userRef= Users.getUser(userName)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding= FragmentCurrentJobsBinding.inflate(inflater,container,false)
        val view=binding.root
        binding.rvFindJobs.adapter= CurrentJobsAdapter(this.requireContext(),userRef!!.getAllAcceptedJobs(), userName)
        binding.btnFjBack.setOnClickListener { back() }


        // binding.btnBack.setOnClickListener { back() }
        //binding.btnSaveProfile.setOnClickListener { submit() }
        //Inflate the layout for this fragment
        return view
    }


    private fun back(){
        findNavController().navigate(
            CurrentJobsDirections.actionCurrentJobsToJobSearcher(
                user = userName
            )
        )
    }
}