package com.revature.healthcareod.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentJobHistoryBinding
import com.revature.healthcareod.datahandlers.*
import com.revature.healthcareod.screens.JobHistoryFragmentDirections
// Job History by Matt

class JobHistoryFragment : Fragment() {

    private var _binding: FragmentJobHistoryBinding?=null
    private val binding get()=_binding!!
    lateinit var userName:String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let{
            userName=it.getString("user").toString()
        }
        if(userName!=null){
            userRef= Users.getUser(userName)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding= FragmentJobHistoryBinding.inflate(inflater,container,false)
        val view=binding.root
        binding.rvJobHistory.adapter= JobAdapter(this.requireContext(),userRef!!.getJobHistory())
        binding.btnJhBack.setOnClickListener { back() }
        var dt=0
        for (j in userRef!!.jobHistory){
            dt+=j.totalAmount
        }
        binding.jhTotalAmountTv.text=dt.toString()


        // binding.btnBack.setOnClickListener { back() }
        //binding.btnSaveProfile.setOnClickListener { submit() }
        //Inflate the layout for this fragment
        return view
    }
    //navigate back
    
    private fun back(){
        findNavController().navigate(
                JobHistoryFragmentDirections.actionJobHistoryFragmentToJobSearcher(
                        user = userName
                )
        )
    }
}