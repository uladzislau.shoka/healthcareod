package com.revature.healthcareod.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentJobSearcherBinding

// Landing page by Matt
/**
 * A simple [Fragment] subclass.
 * Use the [JobSearcher.newInstance] factory method to
 * create an instance of this fragment.
 */
class JobSearcher : Fragment() {
    private var userName:String?=null
    private var _binding: FragmentJobSearcherBinding?=null
    private val binding get()=_binding!!

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        arguments?.let{
            userName=it.getString("user").toString()
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding= FragmentJobSearcherBinding.inflate(inflater,container,false)
        val view=binding.root
        binding.btnLogout.setOnClickListener { logout() }
        binding.btnJsViewAccount.setOnClickListener { toAccDet() }
        binding.btnJsCurrentJobs.setOnClickListener { toCurJob() }
        binding.btnJsFindJobs.setOnClickListener { toFindJob() }
        binding.btnJsSchedule.setOnClickListener { toSchedule() }
        binding.btnJsJobHistory.setOnClickListener { toHistory() }
        //check for not being logged in

        if (userName=="null"){
            logout()
        }

        //Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.textViewHeader.text=getString(R.string.logged_in_string,userName)

        super.onViewCreated(view, savedInstanceState)

    }
    override fun onStart(){
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

    //All these functions just navigate

    private fun logout(){
        findNavController().navigate(R.id.action_jobSearcher_to_FirstFragment)
    }
    private fun toAccDet(){
        //val action =JobSearcherDirections.actionJobSearcherToAccountViewFragment(user =userName)
        findNavController().navigate(
            JobSearcherDirections.actionJobSearcherToAccountViewFragment(
                user = userName ?: ""
            )
        )
    }

    private fun toCurJob(){
        //val action =JobSearcherDirections.actionJobSearcherToAccountViewFragment(user =userName)
        findNavController().navigate(
            JobSearcherDirections.actionJobSearcherToCurrentJobs(
                user = userName ?: ""
            )
        )
    }

    private fun toFindJob(){
        //val action =JobSearcherDirections.actionJobSearcherToAccountViewFragment(user =userName)
        findNavController().navigate(
            JobSearcherDirections.actionJobSearcherToFindJobs(
                user = userName ?:""
            )
        )
    }

    private fun toSchedule(){
        //val action =JobSearcherDirections.actionJobSearcherToAccountViewFragment(user =userName)
        findNavController().navigate(
            JobSearcherDirections.actionJobSearcherToSchedulefragment(
                user = userName ?:""
            )
        )
    }

    private fun toHistory(){
        //val action =JobSearcherDirections.actionJobSearcherToAccountViewFragment(user =userName)
        findNavController().navigate(
            JobSearcherDirections.actionJobSearcherToJobHistoryFragment(
                user = userName ?:""
            )
        )
    }
}