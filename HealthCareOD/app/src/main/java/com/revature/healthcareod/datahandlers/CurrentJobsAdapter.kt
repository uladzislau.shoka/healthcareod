package com.revature.healthcareod.datahandlers

import Models.Job
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R

class CurrentJobsAdapter(private val context: Context, private val dataset: ArrayList<Job>, userName: String):
    RecyclerView.Adapter<CurrentJobsAdapter.ItemViewHolder>()  {
    private val tempUser = Users.getUser(userName)

    class ItemViewHolder(view: View): RecyclerView.ViewHolder(view){
        val h: TextView =view.findViewById(R.id.jh_hospital_name_tv)
        val jn: TextView =view.findViewById(R.id.jh_job_name_tv)
        val d: TextView =view.findViewById(R.id.jh_description_tv)
        val r: TextView =view.findViewById(R.id.jh_rate_tv)
        val t: TextView =view.findViewById(R.id.jh_total_tv)
        val declineButton: Button = view.findViewById(R.id.jh_accept_button)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        println("made it into the job finder")
        val adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.job_finder_element,parent,false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val job=dataset[position]
        holder.declineButton.text = "Decline"
        holder.h.text=DummyData.getHosNameBId(job.hospitalId)
        holder.jn.text=job.jobName
        holder.d.text=job.jobDescription
        holder.r.text=job.ratePerHour.toString()
        holder.t.text=job.totalAmount.toString()
        holder.declineButton.setOnClickListener{
            dataset[position].userId = " "
            dataset[position].status = "Open"
            dataset.remove(dataset[position])
            notifyDataSetChanged()
        }

    }

    override fun getItemCount(): Int {
        return dataset.size
    }

}